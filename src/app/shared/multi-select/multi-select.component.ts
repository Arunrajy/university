import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.css'],

})
export class MultiSelectComponent implements OnInit, DoCheck {
  @Input() data: any = [];
  @Input() dontclose;
  @Input() dontclose1;
  @Output() value: EventEmitter<any> = new EventEmitter();
  @Output() valchange: EventEmitter<any> = new EventEmitter();
  @Output() valchange1: EventEmitter<any> = new EventEmitter();
  show = false;
  arr = [];
  member = new FormControl('');
  _dontclose;
  constructor() { }
  ngDoCheck() {
    if (this.dontclose != undefined) {
      if (this.dontclose == false) {
        this.close();
      }
    }
    if (this.dontclose1 != undefined) {
      if (this.dontclose1 == false) {
        this.close();
      }
    }
  }
  ngOnInit() {

  }
  removeAdd($event: any) {
    const index = this.data.findIndex((ele) => ele.id == $event.id);
    this.data.splice(index, 1);
    this.arr.push($event);
    this.member.patchValue(this.arr);
    if (this.data.length == 0) {
      this.show = false;
    }
    this.value.emit(this.arr);
  }
  showing() {
    this.dontclose = true;
    this.dontclose1 = true;
    this.valchange.emit(this.dontclose);
    this.valchange1.emit(this.dontclose1);
    if (this.data.length == 0) {
      this.show = false;
    } else {
      this.show = true;
    }
  }
  addRemove(ar: any) {
    this.data.push(ar);
    const index = this.arr.findIndex((ele) => ele.id == ar.id);
    this.arr.splice(index, 1);
    this.value.emit(this.arr);
  }
  close() {
    this.show = false;
  }
}
