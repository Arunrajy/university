import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent  implements OnInit  {
  showInner = false;
  show = true;
  @Input() showIn: any;
  @Output() showing: EventEmitter<any> = new EventEmitter();
  constructor() {
   }

  ngOnInit() {
  }
  hideChat() {
    this.showing.emit(!this.show);
  }
}
