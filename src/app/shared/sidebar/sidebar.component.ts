import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
@Input() value: any;
show: any = [];
sidebar
chatShow
  constructor() { }

  ngOnInit() {

  }
  // ===================================================================================================================================
  // SHOWN ELEMENS In SIDE NAVIGATION
  // ==========================================
  Show = (event) => {
    if (this.show[event] == undefined) {
      this.show[event] = true;
      if (this.show.length != 0) {
        this.show.forEach((element, i) => {
          if (event != i) {
            this.show[i] = false;
          }
        });
      }
    } else if (this.show[event] == true && this.show.length != 0) {
      this.show.forEach((element, i) => {
        this.show[i] = false;
      });
    } else {
      if (this.show[event] == false && this.show.length != 0) {
        this.show.forEach((element, i) => {
          if (event == i) {
            this.show[i] = true;
          } else {
            this.show[i] = false;
          }
        });
      }
    }
  }
  
}
