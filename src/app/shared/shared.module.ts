import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ChatComponent } from './chat/chat.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ChatInnerComponent } from './chat-inner/chat-inner.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectComponent } from './multi-select/multi-select.component';
import { NgxMaskModule } from 'ngx-mask';
import { StepPrograssComponent } from './step-prograss/step-prograss.component';
import { StepTabComponent } from './step-tab/step-tab.component';
import { ToastrModule } from 'ngx-toastr';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [ChatComponent, ChatInnerComponent, SidebarComponent, MultiSelectComponent, 
    StepPrograssComponent, StepTabComponent, DeleteModalComponent],
  imports: [
    CommonModule, ChartsModule, FullCalendarModule, PerfectScrollbarModule, RouterModule, FormsModule, ReactiveFormsModule,NgxDatatableModule,
    NgxMaskModule.forRoot(),  ToastrModule.forRoot()
  ],
  providers: [ {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  exports: [
    NgbModule, ChartsModule, FullCalendarModule, ChatComponent, RouterModule, StepPrograssComponent, StepTabComponent,
    SidebarComponent, FormsModule, ReactiveFormsModule, MultiSelectComponent, NgxMaskModule, ToastrModule, DeleteModalComponent,NgxDatatableModule

  ]
})
export class SharedModule { }
