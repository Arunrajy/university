import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  API_URL: any = environment.api_url;
  constructor(public http: HttpClient) { }
  // ============================================================================================
  // GET DATA
  getData(url: any): Observable<any> {
    return this.http.get(`${this.API_URL}${url}`);
  }
  // ============================================================================================
  // GET SINGLE DATA
  getDetail(url: any, id: any): Observable<any> {
    return this.http.get(`${this.API_URL}${url}/${id}`);
  }
  // ============================================================================================
  // POST DATA
  postData(url: any, obj: any): Observable<any> {
    return this.http.post(`${this.API_URL}${url}`, obj);
  }
  // ============================================================================================
  // UPDATE DATA
  updateData(url: any, obj: any, id: any): Observable<any> {
    return this.http.put(`${this.API_URL}${url}/${id}`, obj);
  }
  // ============================================================================================
  // DELETE DATA
  deleteData(url: any, id: any): Observable<any> {
    return this.http.delete(`${this.API_URL}${url}/${id}`);
  }
  // ============================================================================================

}
