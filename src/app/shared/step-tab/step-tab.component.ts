import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';

@Component({
  selector: 'app-step-tab',
  templateUrl: './step-tab.component.html',
  styleUrls: ['./step-tab.component.css']
})
export class StepTabComponent implements OnInit, DoCheck {
  @Input() tab: any = [];
  @Input() tabId: any = [];
  @Output() id: EventEmitter<any> = new EventEmitter();
  selectedItem: any = 1;
  constructor() { }

  ngOnInit() {
  }
  
  ngDoCheck() {
    if (this.selectedItem != this.tabId) {
      this.Select(this.tabId)
    }
  }

  Select(id) {
    this.id.emit(id);
    this.selectedItem = id;
  }
}
