import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepPrograssComponent } from './step-prograss.component';

describe('StepPrograssComponent', () => {
  let component: StepPrograssComponent;
  let fixture: ComponentFixture<StepPrograssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepPrograssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepPrograssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
