import { Component, OnInit, Input, DoCheck } from '@angular/core';

@Component({
  selector: 'app-step-prograss',
  templateUrl: './step-prograss.component.html',
  styleUrls: ['./step-prograss.component.css']
})
export class StepPrograssComponent implements OnInit, DoCheck {
  @Input() data: any;
  showStep = true;
  constructor() { }

  ngOnInit() {
  }
  ngDoCheck() {
    if (this.data != undefined) {
      this.sample(this.data);
    }
  }
  sample(i: any) {
    if (i != undefined) {
      document.getElementsByClassName('prograss_bar')[0].children[i].classList.add('active', 'back')
      if (i == 2) {
        document.getElementsByClassName('prograss_bar')[0].children[0].classList.remove('active', 'back');
        document.getElementsByClassName('prograss_bar')[0].children[1].classList.remove('active', 'back');
        document.getElementsByClassName('prograss_bar')[0].children[2].classList.remove('active', 'back');
      }
    }
  }
}
