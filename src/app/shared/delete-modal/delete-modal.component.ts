import { Component, OnInit, Input, DoCheck, Output, EventEmitter } from '@angular/core';
import { ServiceService } from '../../shared/service.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit, DoCheck {
  @Input() data: any;
  @Output() deleted: EventEmitter<any> = new EventEmitter();
  name: any;
  id: any;
  url: string;
  constructor(private http: ServiceService, public toastr: ToastrService) { }

  ngOnInit() {

  }
  ngDoCheck() {
    if (this.data) {
      this.name = this.data.name;
      this.id = this.data.id;
      this.url = this.data.url;
    }
  }
  close() {
    document.getElementById('myModal').style.display = 'none';
  }
  delete(id: any) {
    this.http.deleteData(this.url, id).subscribe((result) => {
      this.toastr.success('Deleted Successfully');
      this.close();
      this.deleted.emit();
    }, error => console.log(error));
  }
}
