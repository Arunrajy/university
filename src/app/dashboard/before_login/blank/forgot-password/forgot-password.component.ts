import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
forgotForm: FormGroup;
// ===============================================================================
// VALIDATION MESSAGES
// ==============================
validationMessages = {
  'email': [
    { type: 'required', message: 'Email is Required' }
  ] };
  constructor(public fB: FormBuilder) { }

  ngOnInit() {
    this.formInit();
  }
// ===================================================================================
formInit = () => {
  this.forgotForm = this.fB.group({
    email: new FormControl(null, Validators.required)
  });
}

}
