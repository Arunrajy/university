import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankComponent } from './blank/blank.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '', component: BlankComponent, children: [
  { path: 'login', loadChildren: () => import ('./login/login.module').then(m => m.LoginModule)},
  { path: 'signup', loadChildren: () => import ('./signup/signup.module').then(m => m.SignupModule)},
  { path: 'forgot', loadChildren: () => import ('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)},

  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlankRoutingModule { }
