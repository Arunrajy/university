import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
signupForm: FormGroup;
// ===============================================================================
// VALIDATION MESSAGES
// ==============================
validationMessages = {
  'first_name': [
    { type: 'required', message: 'First Name is Required' }
  ],
  'email': [
    { type: 'required', message: 'Email is Required' }
  ],
  'password': [
    { type: 'required', message: 'Password is Required' }
  ]
};
  constructor(public fB: FormBuilder) { }

  ngOnInit() {
    this.formInit();
  }
formInit = () => {
  this.signupForm = this.fB.group({
    first_name: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required)
  });
}
}
