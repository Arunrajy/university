import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm: FormGroup;
// ===============================================================================
// VALIDATION MESSAGES
// ==============================
validationMessages = {
  'email': [
    { type: 'required', message: 'Email is Required' }
  ],
  'password': [
    { type: 'required', message: 'Password is Required' }
  ]
}
  constructor(public fB: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.formInit();
  }
  login(){
    this.router.navigate(['university/dashboard/']);
  }
// ===================================================================================
formInit = () => {
  this.loginForm = this.fB.group({
    email: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required)
  });
}
}
