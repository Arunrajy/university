import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import FroalaEditor from 'froala-editor';
import 'froala-editor/css/third_party/embedly.min.css';
import 'froala-editor/js/third_party/embedly.min.js';
@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MailComponent implements OnInit {
  public imgOptions: object = {
    angularIgnoreAttrs: ['style', 'ng-reflect-froala-editor', 'ng-reflect-froala-model'],
    immediateAngularModelUpdate: true,
    events: {
      'contentChanged': () => {
      }
    }
  };
  compose = false;
  cc = false;
  bcc = false;
content = '<span>My Document\'s Title</span>';

  constructor() { }

  ngOnInit() {
    FroalaEditor.DefineIcon('alert', { SVG_KEY: 'help' });
    FroalaEditor.RegisterCommand('alert', {
      title: 'Hello',
      focus: false,
      undo: false,
      refreshAfterCallback: false,
      callback: function () {
        alert('Hello!');
      }
    });
  }

}
