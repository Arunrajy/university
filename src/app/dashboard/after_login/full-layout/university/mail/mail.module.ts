import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MailRoutingModule } from './mail-routing.module';
import { MailComponent } from './mail/mail.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
  declarations: [MailComponent, ],
  imports: [
    CommonModule,
    MailRoutingModule, FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
  ]
})
export class MailModule { }
