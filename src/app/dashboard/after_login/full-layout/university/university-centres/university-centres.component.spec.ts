import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversityCentresComponent } from './university-centres.component';

describe('UniversityCentresComponent', () => {
  let component: UniversityCentresComponent;
  let fixture: ComponentFixture<UniversityCentresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityCentresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversityCentresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
