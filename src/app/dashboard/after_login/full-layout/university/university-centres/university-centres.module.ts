import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UniversityCentresRoutingModule } from './university-centres-routing.module';
import { UniversityCentresComponent } from './university-centres.component';
import { SharedModule } from '../../../../../shared/shared.module';

@NgModule({
  declarations: [UniversityCentresComponent],
  imports: [
    CommonModule,
    UniversityCentresRoutingModule, SharedModule
  ]
})
export class UniversityCentresModule { }
