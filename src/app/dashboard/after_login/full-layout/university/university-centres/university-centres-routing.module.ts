import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UniversityCentresComponent } from './university-centres.component';

const routes: Routes = [
  { path: '', component: UniversityCentresComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UniversityCentresRoutingModule { }
