import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentAddRoutingModule } from './department-add-routing.module';
import { DepartmentAddComponent } from './department-add.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [DepartmentAddComponent],
  imports: [
    CommonModule,
    DepartmentAddRoutingModule,SharedModule
  ]
})
export class DepartmentAddModule { }
