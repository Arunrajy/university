import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.css']
})
export class DepartmentAddComponent implements OnInit {

  constructor(private FB: FormBuilder) { }
  departmentAddform: FormGroup;
  showBasic: Boolean = true;
  showInfo: Boolean = true;
  showContact: Boolean = true;
  ngOnInit() {
    this.FormInit();
  }
  //--------------------------------------------------=FormInit=---------------------------------------------
  FormInit() {
    this.departmentAddform = this.FB.group({
      basic_info: this.FB.group({
        department_name: new FormControl(null, Validators.required),
        head_of_department: new FormControl(null, Validators.required),
        no_of_students: new FormControl(null, Validators.required),
        brief: new FormControl(null, Validators.required),
      }),
      account_info: this.FB.group({
        email: new FormControl(null, Validators.required),
        phone: new FormControl(null, Validators.required),
      }),
    })
  }
  //=====================================================================================
  //-----------------------------------=Validation=-------------------------------------
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.departmentAddform.get(field).valid && this.departmentAddform.get(field).touched;
  }
  isFileValid(field: string) {
    return !this.departmentAddform.get(field).valid;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validation_messages = {
    'department_name': [
      { type: 'required', message: 'Department Name is required.' }
    ],
    'head_of_department': [
      { type: 'required', message: 'Head Of Department is required.' }
    ],
    'no_of_students': [
      { type: 'required', message: 'No Of Students is required.' }
    ],
    'brief': [
      { type: 'required', message: 'Brief is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required.' }
    ],
  }
  //=====================================================================================
  //-----------------------------------------------=Action=--------------------------------
  submit() {
    console.log(this.departmentAddform.value);
    if (this.departmentAddform.valid) {
      console.log(this.departmentAddform.get('basic_info').value);
      console.log(this.departmentAddform.get('account_info').value);
    }
    else {
      console.log("else")
      this.validateAllFormFields(this.departmentAddform);
    }

  }
  reset() {
    this.departmentAddform.reset();
  }
  //===============================================================================================
}
