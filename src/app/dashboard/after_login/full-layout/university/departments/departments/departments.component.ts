import { Component, OnInit , ViewChild} from '@angular/core';

declare var require: any;
const data: any = require('./departments.json');
@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  rows = [];
  temp = [...data];
  
  @ViewChild(DepartmentsComponent, {static: true}) table: DepartmentsComponent;
  constructor() {
    this.rows = data;
    this.temp = [...data];
   }
  showList:Boolean=true;
  page=3;
  ngOnInit() {
  }
  //search filter
  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.department.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = data;
  }

}
