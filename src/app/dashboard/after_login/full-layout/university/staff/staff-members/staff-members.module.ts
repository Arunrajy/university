import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffMembersRoutingModule } from './staff-members-routing.module';
import { StaffMembersComponent } from './staff-members.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [StaffMembersComponent],
  imports: [
    CommonModule,
    StaffMembersRoutingModule, SharedModule
  ]
})
export class StaffMembersModule { }
