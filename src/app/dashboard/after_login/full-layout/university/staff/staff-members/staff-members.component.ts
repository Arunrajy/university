import { Component, OnInit } from '@angular/core';
declare var require: any;
const data: any = require('./staff-members.json');
@Component({
  selector: 'app-staff-members',
  templateUrl: './staff-members.component.html',
  styleUrls: ['./staff-members.component.css']
})
export class StaffMembersComponent implements OnInit {
  ShowList: Boolean = true;
  page = 1;
  staffMembers:any = [];
  constructor() { }

  ngOnInit() {
    this.getStaffMembers();
  }

  getStaffMembers(){
    this.staffMembers = data.staffMembers;
  }
}
