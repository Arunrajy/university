import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffMembersEditRoutingModule } from './staff-members-edit-routing.module';
import { StaffMembersEditComponent } from './staff-members-edit.component';


@NgModule({
  declarations: [StaffMembersEditComponent],
  imports: [
    CommonModule,
    StaffMembersEditRoutingModule
  ]
})
export class StaffMembersEditModule { }
