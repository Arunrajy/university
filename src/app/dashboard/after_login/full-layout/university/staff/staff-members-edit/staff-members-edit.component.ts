import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff-members-edit',
  templateUrl: './staff-members-edit.component.html',
  styleUrls: ['./staff-members-edit.component.css']
})
export class StaffMembersEditComponent implements OnInit {

  constructor() { }
  showBasic: Boolean = true;
  showInfo: Boolean = true;
  showContact: Boolean = true;
  ngOnInit() {
  }

}
