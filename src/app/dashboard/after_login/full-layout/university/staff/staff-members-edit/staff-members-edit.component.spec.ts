import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffMembersEditComponent } from './staff-members-edit.component';

describe('StaffMembersEditComponent', () => {
  let component: StaffMembersEditComponent;
  let fixture: ComponentFixture<StaffMembersEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffMembersEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffMembersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
