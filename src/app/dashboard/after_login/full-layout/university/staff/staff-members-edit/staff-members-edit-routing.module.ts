import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffMembersEditComponent } from './staff-members-edit.component';

const routes: Routes = [
  { path: '', component: StaffMembersEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffMembersEditRoutingModule { }
