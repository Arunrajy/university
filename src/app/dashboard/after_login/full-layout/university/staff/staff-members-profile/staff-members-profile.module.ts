import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffMembersProfileRoutingModule } from './staff-members-profile-routing.module';
import { StaffMembersProfileComponent } from './staff-members-profile.component';


@NgModule({
  declarations: [StaffMembersProfileComponent],
  imports: [
    CommonModule,
    StaffMembersProfileRoutingModule
  ]
})
export class StaffMembersProfileModule { }
