import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffMembersProfileComponent } from './staff-members-profile.component';

describe('StaffMembersProfileComponent', () => {
  let component: StaffMembersProfileComponent;
  let fixture: ComponentFixture<StaffMembersProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffMembersProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffMembersProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
