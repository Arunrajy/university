import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffMembersProfileComponent } from './staff-members-profile.component';

const routes: Routes = [
  { path: '', component: StaffMembersProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffMembersProfileRoutingModule { }
