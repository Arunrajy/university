import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffMembersAddComponent } from './staff-members-add.component';

const routes: Routes = [
  { path: '', component: StaffMembersAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffMembersAddRoutingModule { }
