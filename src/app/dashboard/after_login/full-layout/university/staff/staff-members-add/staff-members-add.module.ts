import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffMembersAddRoutingModule } from './staff-members-add-routing.module';
import { StaffMembersAddComponent } from './staff-members-add.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [StaffMembersAddComponent],
  imports: [
    CommonModule,
    StaffMembersAddRoutingModule,SharedModule
  ]
})
export class StaffMembersAddModule { }
