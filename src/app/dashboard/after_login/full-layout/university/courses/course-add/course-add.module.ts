import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseAddRoutingModule } from './course-add-routing.module';
import { CourseAddComponent } from './course-add.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [CourseAddComponent],
  imports: [
    CommonModule,
    CourseAddRoutingModule,SharedModule
  ]
})
export class CourseAddModule { }
