import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
@Component({
  selector: 'app-course-add',
  templateUrl: './course-add.component.html',
  styleUrls: ['./course-add.component.css']
})
export class CourseAddComponent implements OnInit {
  constructor(private FB: FormBuilder) { }
  courseAddform: FormGroup;
  showBasic: Boolean = true;
  showInfo: Boolean = true;
  showContact: Boolean = true;
  ngOnInit() {
    this.FormInit();
  }
  //--------------------------------------------------=FormInit=---------------------------------------------
  FormInit() {
    this.courseAddform = this.FB.group({
      basic_info: this.FB.group({
        name: new FormControl(null, Validators.required),
        starts_from: new FormControl(null, Validators.required),
        course_time_length: new FormControl(null, Validators.required),
        course_price: new FormControl(null, Validators.required),
        professor: new FormControl(null, Validators.required),
        course_image: new FormControl(null, Validators.required),
        year: new FormControl(null, Validators.required),
        department: new FormControl(null, Validators.required),
        brief: new FormControl(null, Validators.required),
        website_url: new FormControl(null, Validators.required),
      }),
      social_media_info: this.FB.group({
        facebook_url: new FormControl(null, Validators.required),
        twitter_url: new FormControl(null, Validators.required),
        google_plus_url: new FormControl(null, Validators.required),
      }),
    })
  }
  //=====================================================================================
  //-----------------------------------=Validation=-------------------------------------
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.courseAddform.get(field).valid && this.courseAddform.get(field).touched;
  }
  isFileValid(field: string) {
    return !this.courseAddform.get(field).valid;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'starts_from': [
      { type: 'required', message: 'Starts From is required.' }
    ],
    'course_time_length': [
      { type: 'required', message: 'Course Time Length is required.' }
    ],
    'course_image': [
      { type: 'required', message: 'Course Image is required.' }
    ],
    'department': [
      { type: 'required', message: 'Department is required.' }
    ],
    'course_price': [
      { type: 'required', message: 'Course Price is required.' }
    ],
    'brief': [
      { type: 'required', message: 'Brief is required.' }
    ],
    'website_url': [
      { type: 'required', message: 'Website URL is required.' }
    ],
    'year': [
      { type: 'required', message: 'Year is required.' }
    ],
    'professor': [
      { type: 'required', message: 'Professor is required.' }
    ],
    'facebook_url': [
      { type: 'required', message: 'Facebook URL is required.' }
    ],
    'twitter_url': [
      { type: 'required', message: 'Twitter URL is required.' }
    ],
    'google_plus_url': [
      { type: 'required', message: 'Google Plus URL is required.' }
    ],
  }
  //=====================================================================================
  //-----------------------------------------------=Action=--------------------------------
  submit() {
    console.log(this.courseAddform.value);
    if (this.courseAddform.valid) {
      console.log(this.courseAddform.get('basic_info').value);
      console.log(this.courseAddform.get('account_info').value);
      console.log(this.courseAddform.get('social_media_info').value);
    }
    else {
      console.log("else")
      this.validateAllFormFields(this.courseAddform);
    }

  }
  reset() {
    this.courseAddform.reset();
  }
  //===============================================================================================
}
