import { Component, OnInit } from '@angular/core';
declare var require: any;
const data: any = require('./courses.json');
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  page = 1;
  ShowList: Boolean = true;
  courses:any=[];
  constructor() { }

  
  ngOnInit() {
    this.getCoursesList();
  }
  getCoursesList(){
    this.courses = data.courses;
  }
}
