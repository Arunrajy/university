import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseEditRoutingModule } from './course-edit-routing.module';
import { CourseEditComponent } from './course-edit.component';


@NgModule({
  declarations: [CourseEditComponent],
  imports: [
    CommonModule,
    CourseEditRoutingModule
  ]
})
export class CourseEditModule { }
