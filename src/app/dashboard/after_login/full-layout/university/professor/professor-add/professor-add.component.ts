import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../../../../../shared/service.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
@Component({
  selector: 'app-professor-add',
  templateUrl: './professor-add.component.html',
  styleUrls: ['./professor-add.component.css']
})
export class ProfessorAddComponent implements OnInit {

  constructor(private FB: FormBuilder, private http: ServiceService, private ActivatedRoute: ActivatedRoute, private toastr: ToastrService, private location: Location) {
    if (this.ActivatedRoute.snapshot.queryParams.id != undefined) {
      // this.Edit(this.ActivatedRoute.snapshot.queryParams.id);
    }
  }
  Url = "professor";
  professorAddform: FormGroup;
  showBasic: Boolean = true;
  showInfo: Boolean = false;
  showContact: Boolean = false;
  tabId = 1;
  updateBtn: Boolean = false;
  imageURI: any;
  ngOnInit() {
    this.FormInit();
  }
  //--------------------------------------------------=FormInit=---------------------------------------------
  FormInit() {
    this.professorAddform = this.FB.group({
      basic_info: this.FB.group({
        name: new FormControl(null, Validators.required),
        date_of_birth: new FormControl(null, Validators.required),
        gender: new FormControl(null, Validators.required),
        profile_image: new FormControl(''),
        department: new FormControl(null, Validators.required),
        position: new FormControl(null, Validators.required),
        brief: new FormControl(null, Validators.required),
        website_url: new FormControl(null, Validators.required),
      }),
      account_info: this.FB.group({
        email: new FormControl(null, [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]),
        phone: new FormControl(null, [Validators.required, Validators.maxLength(10), Validators.minLength(10)]),
        password: new FormControl(null, Validators.required),
        confirm_password: new FormControl(null, Validators.required),
      }),
      social_media_info: this.FB.group({
        facebook_url: new FormControl(null, Validators.required),
        twitter_url: new FormControl(null, Validators.required),
        google_plus_url: new FormControl(null, Validators.required),
      }),
    })
  }
  //=====================================================================================
  //-----------------------------------=Validation=-------------------------------------
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.professorAddform.get(field).valid && this.professorAddform.get(field).touched;
  }
  isFileValid(field: string) {
    return !this.professorAddform.get(field).valid;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'date_of_birth': [
      { type: 'required', message: 'Date Of Birth is required.' }
    ],
    'gender': [
      { type: 'required', message: 'Gender is required.' }
    ],
    'profile_image': [
      { type: 'required', message: 'Profile Image is required.' }
    ],
    'department': [
      { type: 'required', message: 'Department is required.' }
    ],
    'position': [
      { type: 'required', message: 'Position is required.' }
    ],
    'brief': [
      { type: 'required', message: 'Brief is required.' }
    ],
    'website_url': [
      { type: 'required', message: 'Website URL is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter Valid Email.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'maxlength', message: 'Enter Valid Phone Number.' },
      { type: 'minlength', message: 'Enter Valid Phone Number.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm Password is required.' }
    ],
    'facebook_url': [
      { type: 'required', message: 'Facebook URL is required.' }
    ],
    'twitter_url': [
      { type: 'required', message: 'Twitter URL is required.' }
    ],
    'google_plus_url': [
      { type: 'required', message: 'Google Plus URL is required.' }
    ],
  }
  // =====================================================================================
  // -----------------------------------------------=Action=--------------------------------
  reset() {
    this.professorAddform.reset();
    this.tabId = 1;
    this.updateBtn = false;
  }
  submit() {
    console.log(this.professorAddform.value);

    if (this.professorAddform.valid) {
      console.log(this.professorAddform.get('basic_info').value);
      console.log(this.professorAddform.get('account_info').value);
      console.log(this.professorAddform.get('social_media_info').value);
      this.professorAddform.value.basic_info.profile_image = this.imageURI;
      this.http.postData(this.Url, this.professorAddform.value).subscribe((result) => {
        if (!result.id) {

        } else {
          this.reset();
        }

        // }
        // else {
        this.toastr.success("Professor added Successfully")
        this.reset();
        // }
      });
    } else {
      this.validateAllFormFields(this.professorAddform);
      this.toastr.error("Please Fill all Required fields");
    }
  }
  Edit(id) {
    this.http.getDetail(this.Url, id).subscribe((result) => {
      console.log(result);
      this.professorAddform.controls.basic_info.patchValue({
        name: result.basic_info.name,
        date_of_birth: result.basic_info.date_of_birth,
        gender: result.basic_info.gender,
        // profile_image: (result.basic_info.profile_image, { emitModelToViewChange: false }),
        department: result.basic_info.department,
        position: result.basic_info.position,
        brief: result.basic_info.brief,
        website_url: result.basic_info.website_url,
      });
      this.professorAddform.controls.account_info.patchValue({
        email: result.account_info.email,
        phone: result.account_info.phone,
        password: result.account_info.password,
        confirm_password: result.account_info.confirm_password,
      })
      this.professorAddform.controls.social_media_info.patchValue({
        facebook_url: result.social_media_info.facebook_url,
        twitter_url: result.social_media_info.twitter_url,
        google_plus_url: result.social_media_info.google_plus_url,
      })
      this.imageURI = result.basic_info.profile_image;
    })

    this.updateBtn = true;
  }
  Update() {
    this.professorAddform.value.basic_info.profile_image = this.imageURI;
    if (this.professorAddform.valid) {
      this.http.updateData(this.Url, this.professorAddform.value, this.ActivatedRoute.snapshot.queryParams.id).subscribe((result) => {
        this.toastr.success("Professor updated Successfully");
        this.reset();
        this.location.back();
      })
    }
    else {
      this.validateAllFormFields(this.professorAddform);
      this.toastr.error("Please Fill all Required fields");
    }

  }
  //===============================================================================================

  nextToaccountInfo() {
    if (this.professorAddform.controls.basic_info.valid) {
      this.showAccounttab();
    }
    else {
      this.validateAllFormFields(this.professorAddform.controls.basic_info as FormGroup);
    }
  }
  resetBasic() {
    this.professorAddform.get('basic_info').reset();
    this.imageURI = 0;
  }
  nextToContact() {
    if (this.professorAddform.controls.account_info.valid) {
      this.showContacttab();
    }
    else {
      this.validateAllFormFields(this.professorAddform.controls.account_info as FormGroup);
    }
  }
  resetAccount() {
    this.professorAddform.get('account_info').reset();
  }
  resetMedia() {
    this.professorAddform.get('social_media_info').reset();
  }
  //====================================================================

  Tab = [
    { id: 1, heading: "Professor Basic Info" },
    { id: 2, heading: "Professor Account Info" },
    { id: 3, heading: "Professor Social Media Info" },
  ]
  selectedTab(id) {
    if (id == 2) {
      this.showAccounttab();
    }
    else if (id == 3) {
      this.showContacttab();
    } else {
      this.showBasictab();
    }
  }
  showBasictab() {
    this.professorAddform.controls.basic_info.get('profile_image').reset();
    this.showBasic = true;
    this.showInfo = false;
    this.showContact = false;
    this.tabId = 1;
  }
  showAccounttab() {
    this.showBasic = false;
    this.showInfo = true;
    this.showContact = false;
    this.tabId = 2;
  }
  showContacttab() {
    this.showBasic = false;
    this.showInfo = false;
    this.showContact = true;
    this.tabId = 3;
  }
  handleCategoryBanner(files: any) {
    const reader = new FileReader();
    reader.readAsDataURL(files.target.files[0]);
    reader.onload = () => {
      this.imageURI = reader.result;
    };
  }
}