import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfessorAddComponent } from './professor-add.component';

const routes: Routes = [
  { path: '', component: ProfessorAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessorAddRoutingModule { }
