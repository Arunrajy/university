import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessorAddRoutingModule } from './professor-add-routing.module';
import { ProfessorAddComponent } from './professor-add.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [ProfessorAddComponent],
  imports: [
    CommonModule,
    ProfessorAddRoutingModule, SharedModule
  ]
})
export class ProfessorAddModule { }
