import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ServiceService } from '../../../../../../shared/service.service';
declare var require: any;
const data: any = require('./professors.json');
@Component({
  selector: 'app-professors',
  templateUrl: './professors.component.html',
  styleUrls: ['./professors.component.css']
})
export class ProfessorsComponent implements OnInit {

  constructor(private http: ServiceService, private router: Router) { }
  Url = "professor";
  professors: any = [];
  ShowList: Boolean = true;
  page = 1;
  limit = 6;
  ngOnInit() {
    this.getList();
  }
  pageChange = (event: any) => {
    console.log(event)
    this.page = event;
    this.getList();
  }
  //--------------------------------=Action=------------------------
  getList() {
    // this.http.getData(`${this.Url}?_page=${this.page}&_limit=${this.limit}`).subscribe((result) => {
    //   this.professors = result;
    // })
    this.professors = data.professor;
  }

  professorEdit(id) {
    this.router.navigate(['/university/professor-add'], { queryParams: { 'id': id } })
  }
  //====================================================================
}
