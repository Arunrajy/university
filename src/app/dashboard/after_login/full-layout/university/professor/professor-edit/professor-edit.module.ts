import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessorEditRoutingModule } from './professor-edit-routing.module';
import { ProfessorEditComponent } from './professor-edit.component';


@NgModule({
  declarations: [ProfessorEditComponent],
  imports: [
    CommonModule,
    ProfessorEditRoutingModule
  ]
})
export class ProfessorEditModule { }
