import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfessorEditComponent } from './professor-edit.component';

const routes: Routes = [
  { path: '', component: ProfessorEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessorEditRoutingModule { }
