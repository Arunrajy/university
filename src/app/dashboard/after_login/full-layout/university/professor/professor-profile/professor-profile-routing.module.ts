import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfessorProfileComponent } from './professor-profile.component'

const routes: Routes = [
  { path: '', component: ProfessorProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessorProfileRoutingModule { }
