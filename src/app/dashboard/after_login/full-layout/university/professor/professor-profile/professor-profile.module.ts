import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessorProfileRoutingModule } from './professor-profile-routing.module';
import { ProfessorProfileComponent } from './professor-profile.component';


@NgModule({
  declarations: [ProfessorProfileComponent],
  imports: [
    CommonModule,
    ProfessorProfileRoutingModule
  ]
})
export class ProfessorProfileModule { }
