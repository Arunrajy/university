import { Component, OnInit } from '@angular/core';
declare var require: any;
const data: any = require('./students.json');
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  ShowList:Boolean=true;
  page=1;
  students:any=[];
  constructor() { }

  ngOnInit() {
    this.getStudentList();
  }
  getStudentList(){
    this.students = data.students;
  }
}
