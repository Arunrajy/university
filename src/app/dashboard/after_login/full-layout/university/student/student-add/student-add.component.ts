import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
@Component({
  selector: 'app-student-add',
  templateUrl: './student-add.component.html',
  styleUrls: ['./student-add.component.css']
})
export class StudentAddComponent implements OnInit {

  constructor(private FB: FormBuilder) { }
  studentAddform: FormGroup;
  showBasic: Boolean = true;
  showInfo: Boolean = true;
  showContact: Boolean = true;
  ngOnInit() {
    this.FormInit();
  }
  //--------------------------------------------------=FormInit=---------------------------------------------
  FormInit() {
    this.studentAddform = this.FB.group({
      basic_info: this.FB.group({
        name: new FormControl(null, Validators.required),
        date_of_birth: new FormControl(null, Validators.required),
        gender: new FormControl(null, Validators.required),
        profile_image: new FormControl(null, Validators.required),
        department: new FormControl(null, Validators.required),
        position: new FormControl(null, Validators.required),
        brief: new FormControl(null, Validators.required),
        website_url: new FormControl(null, Validators.required),
      }),
      account_info: this.FB.group({
        email: new FormControl(null, Validators.required),
        phone: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.required),
        confirm_password: new FormControl(null, Validators.required),
      }),
      social_media_info: this.FB.group({
        facebook_url: new FormControl(null, Validators.required),
        twitter_url: new FormControl(null, Validators.required),
        google_plus_url: new FormControl(null, Validators.required),
      }),
    })
  }
  //=====================================================================================
  //-----------------------------------=Validation=-------------------------------------
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.studentAddform.get(field).valid && this.studentAddform.get(field).touched;
  }
  isFileValid(field: string) {
    return !this.studentAddform.get(field).valid;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'date_of_birth': [
      { type: 'required', message: 'Date Of Birth is required.' }
    ],
    'gender': [
      { type: 'required', message: 'Gender is required.' }
    ],
    'profile_image': [
      { type: 'required', message: 'Profile Image is required.' }
    ],
    'department': [
      { type: 'required', message: 'Department is required.' }
    ],
    'position': [
      { type: 'required', message: 'Position is required.' }
    ],
    'brief': [
      { type: 'required', message: 'Brief is required.' }
    ],
    'website_url': [
      { type: 'required', message: 'Website URL is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm Password is required.' }
    ],
    'facebook_url': [
      { type: 'required', message: 'Facebook URL is required.' }
    ],
    'twitter_url': [
      { type: 'required', message: 'Twitter URL is required.' }
    ],
    'google_plus_url': [
      { type: 'required', message: 'Google Plus URL is required.' }
    ],
  }
  //=====================================================================================
  //-----------------------------------------------=Action=--------------------------------
  submit() {
    console.log(this.studentAddform.value);
    if (this.studentAddform.valid) {
      console.log(this.studentAddform.get('basic_info').value);
      console.log(this.studentAddform.get('account_info').value);
      console.log(this.studentAddform.get('social_media_info').value);
    }
    else {
      console.log("else")
      this.validateAllFormFields(this.studentAddform);
    }

  }
  reset() {
    this.studentAddform.reset();
  }
  //===============================================================================================
}
