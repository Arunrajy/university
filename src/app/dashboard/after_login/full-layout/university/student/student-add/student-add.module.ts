import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentAddRoutingModule } from './student-add-routing.module';
import { StudentAddComponent } from './student-add.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [StudentAddComponent],
  imports: [
    CommonModule,
    StudentAddRoutingModule, SharedModule
  ]
})
export class StudentAddModule { }
