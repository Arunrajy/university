import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
@Component({
  selector: 'app-library-assets-edit',
  templateUrl: './library-assets-edit.component.html',
  styleUrls: ['./library-assets-edit.component.css']
})
export class LibraryAssetsEditComponent implements OnInit {

  constructor(private FB: FormBuilder) { }
  libraryAssetsedit: FormGroup;
  showBasic: Boolean = true;
  ngOnInit() {
    this.FormInit();
  }

  //-----------------------------------=Form Init=-------------------------------------
  FormInit() {
    this.libraryAssetsedit = this.FB.group({
      title: new FormControl(null, Validators.required),
      subject: new FormControl(null, Validators.required),
      libraryassetsprice: new FormControl(null, Validators.required),
      author: new FormControl(null, Validators.required),
      department_id: new FormControl(null, Validators.required),
      asset_type_id: new FormControl(null, Validators.required),
      status_id: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required),
      brief: new FormControl(null, Validators.required),
    })
  }
  //=====================================================================================
  //-----------------------------------=Validation=-------------------------------------
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.libraryAssetsedit.get(field).valid && this.libraryAssetsedit.get(field).touched;
  }
  isFileValid(field: string) {
    return !this.libraryAssetsedit.get(field).valid;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validation_messages = {
    'title': [
      { type: 'required', message: 'Title is required.' }
    ],
    'libraryassetsprice': [
      { type: 'required', message: 'Library Assets Price is required.' }
    ],
    'subject': [
      { type: 'required', message: 'Subject is required.' }
    ],
    'author': [
      { type: 'required', message: 'Author is required.' }
    ],
    'department_id': [
      { type: 'required', message: 'Department is required.' }
    ],
    'asset_type_id': [
      { type: 'required', message: 'Asset Type is required.' }
    ],
    'status_id': [
      { type: 'required', message: 'Status is required.' }
    ],
    'year': [
      { type: 'required', message: 'Year is required.' }
    ],
    'brief': [
      { type: 'required', message: 'Brief is required.' }
    ],
  }
  //=====================================================================================
  //-----------------------------------=Action=-------------------------------------
  submit() {
    if (this.libraryAssetsedit.valid) {
      console.log(this.libraryAssetsedit.value)
    } else {
      this.validateAllFormFields(this.libraryAssetsedit);
    }

  }
  reset() {
    this.libraryAssetsedit.reset();
  }
  //=====================================================================================

}
