import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibraryAssetsEditComponent } from './library-assets-edit.component';

describe('LibraryAssetsEditComponent', () => {
  let component: LibraryAssetsEditComponent;
  let fixture: ComponentFixture<LibraryAssetsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibraryAssetsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibraryAssetsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
