import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryAssetsEditRoutingModule } from './library-assets-edit-routing.module';
import { LibraryAssetsEditComponent } from './library-assets-edit.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [LibraryAssetsEditComponent],
  imports: [
    CommonModule,
    LibraryAssetsEditRoutingModule,SharedModule
  ]
})
export class LibraryAssetsEditModule { }
