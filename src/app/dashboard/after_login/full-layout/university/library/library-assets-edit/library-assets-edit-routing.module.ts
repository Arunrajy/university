import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryAssetsEditComponent } from './library-assets-edit.component';

const routes: Routes = [
  { path: '', component: LibraryAssetsEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibraryAssetsEditRoutingModule { }
