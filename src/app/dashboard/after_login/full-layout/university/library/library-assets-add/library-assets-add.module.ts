import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryAssetsAddRoutingModule } from './library-assets-add-routing.module';
import { LibraryAssetsAddComponent } from './library-assets-add.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [LibraryAssetsAddComponent],
  imports: [
    CommonModule,
    LibraryAssetsAddRoutingModule,SharedModule
  ]
})
export class LibraryAssetsAddModule { }
