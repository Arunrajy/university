import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibraryAssetsAddComponent } from './library-assets-add.component';

describe('LibraryAssetsAddComponent', () => {
  let component: LibraryAssetsAddComponent;
  let fixture: ComponentFixture<LibraryAssetsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibraryAssetsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibraryAssetsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
