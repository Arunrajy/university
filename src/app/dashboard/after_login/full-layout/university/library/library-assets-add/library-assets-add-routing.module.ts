import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryAssetsAddComponent } from './library-assets-add.component';

const routes: Routes = [
  { path: '', component: LibraryAssetsAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibraryAssetsAddRoutingModule { }
