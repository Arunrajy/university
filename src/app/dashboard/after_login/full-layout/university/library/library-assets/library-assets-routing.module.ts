import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryAssetsComponent } from './library-assets.component';

const routes: Routes = [
  { path: '', component: LibraryAssetsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibraryAssetsRoutingModule { }
