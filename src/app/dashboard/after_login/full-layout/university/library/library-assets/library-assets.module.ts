import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryAssetsRoutingModule } from './library-assets-routing.module';
import { LibraryAssetsComponent } from './library-assets.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [LibraryAssetsComponent],
  imports: [
    CommonModule,
    LibraryAssetsRoutingModule,SharedModule
  ]
})
export class LibraryAssetsModule { }
