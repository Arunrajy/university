import { Component, ViewChild, OnInit } from '@angular/core';

declare var require: any;
const data: any = require('./library-assets.json');
@Component({
  selector: 'app-library-assets',
  templateUrl: './library-assets.component.html',
  styleUrls: ['./library-assets.component.css']
})
export class LibraryAssetsComponent implements OnInit {
  rows = [];
  temp = [...data];
  
  @ViewChild(LibraryAssetsComponent, {static: true}) table: LibraryAssetsComponent;
  constructor() {
    this.rows = data;
    this.temp = [...data];
   }
  showList= true;
  page = 3;
  ngOnInit() {
  }
  //search filter
  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = data;
  }

}
