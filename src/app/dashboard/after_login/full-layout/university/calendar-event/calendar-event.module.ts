import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarEventRoutingModule } from './calendar-event-routing.module';
import { CalendarEventComponent } from './calendar-event/calendar-event.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [CalendarEventComponent],
  imports: [
    CommonModule, SharedModule, PerfectScrollbarModule,
    CalendarEventRoutingModule
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class CalendarEventModule { }
