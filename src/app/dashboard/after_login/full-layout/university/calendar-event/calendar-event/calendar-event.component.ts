import { Component, ViewChild, OnInit } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput, } from '@fullcalendar/core';
import enGbLocale from '@fullcalendar/core/locales/en-gb';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { FormBuilder, Validator, FormArray, FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.css']
})
export class CalendarEventComponent implements OnInit {
  @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template

  calendarVisible = true;
  calendarWeekends = true;
  dayGridMonth: any;
  defaultView = 'timeGridWeek';
  public locales = [enGbLocale];
  arr = [];

  calendarEvents: EventInput[] =
    [
      { id: 1, title: 'Event 1', date: '2019-12-19' },
      { id: 2, title: 'Event 2', date: '2019-12-19T14:30:00' },
      { id: 3, title: 'Event 3', date: new Date() },
      { id: 4, title: 'Event 4', date: '2019-12-04' }
    ];

  form: FormGroup;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];

  constructor(public Fb: FormBuilder) {

  }
  dateRender($event) {

  }
  // ====================================================================================================
  // DOUBLE CLICKING DATE
  // ==========================
  days($event: any) {
    if (this.arr.length === 0) {
      this.arr.push($event.dateStr);
    } else {
      if (this.arr[0] === $event.dateStr) {
        const control = this.form.get('events') as FormArray;
        control.push(this.Fb.group({ id: (control.length) + 1, title: 'New Event', date: new Date($event.dateStr) }));
        this.calendarEvents = control.value;
        this.arr = [];
      } else {
        this.arr = [];
      }
    }
  }
  // ======================================================================================================
  // DRAGGING ELEMENYT
  changing($event: any) {
    const control = this.form.get('events') as FormArray;
    console.log(control.value)
    const index = this.calendarEvents.findIndex((element: any) => element.id == $event.oldEvent.id);
    control.removeAt(index);
    control.push(this.Fb.group({ id: $event.oldEvent.id, title: $event.oldEvent.title, date: new Date($event.event.start) }));
    this.calendarEvents = control.value;
  }
  // ======================================================================================================
  changingValue($event: any, i: any) {
    const control = this.form.get('events') as FormArray;
    control.at(i).get('title').patchValue($event.target.value);
    this.calendarEvents = control.value;
  }
  ngOnInit() {
    this.formInit();
    this.pushValue();

  }
  // ======================================================================================================
  // CALENDAR METHODS
  // =======================
  formInit = () => {
    this.form = this.Fb.group({
      events: this.Fb.array([])

    });
  }
  pushValue = () => {
    const control = this.form.get('events') as FormArray;
    while (control.length != 0) {
      control.removeAt(0);
    }
    if (this.calendarEvents.length != 0) {
      this.calendarEvents.forEach(elem => {
        control.push(this.Fb.group({
          id: elem.id,
          title: elem.title,
          date: elem.date
        }));
      });
    }
  }
  OnEventsInit() {
    return this.Fb.group({
      id: new FormControl(null),
      title: new FormControl('New Event'),
      date: new FormControl(new Date())
    });
  }
  removeItem(i: any) {
    const control = this.form.get('events') as FormArray;
    control.removeAt(i);
    this.calendarEvents = control.value;
  }
  addEvent = () => {
    const control = this.form.get('events') as FormArray;
    control.push(this.Fb.group({ id: (this.calendarEvents.length) + 1, title: 'New Event', date: new Date() }));
    this.calendarEvents = control.value;

  }
  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  }

  handleDateClick(arg) {
    if (confirm('Would you like to add an event to ' + arg.dateStr + ' ?')) {
      this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
        title: 'New Event',
        start: arg.date,
        allDay: arg.allDay
      });
    }
  }
  changeView = (event: any) => {
    this.dayGridMonth = event;
    console.log(event)
  }
  // ====================================================================================================================================
}
