import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, RadialChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective, } from 'ng2-charts';
import '../../../../../../../node_modules/jquery-sparkline/jquery.sparkline.min.js';
declare var jQuery: any;

declare var require: any;
const data: any = require('./dashboard.json');
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  showAdmission = true;
  showToppers = true;
  showPassing = true;
  showProgress = true;
  showSubject = true;
  showAttendance = true;
  showActivity = true;
  showPercent = true;
  constructor() { 
    console.log(data);
  }

  //linechart 1
  public lineChartLegend1 = true;
  public lineChartType1 = 'line';
  // MONTHLY SURVEY
  public lineChartColors1: Color[] = data.lineChartData.colors;
  public lineChartData1: ChartDataSets[] = data.lineChartData.datasets;
  public lineChartLabels1: Label[] = data.lineChartData.labels;

  public lineChartOptions1: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    elements: {
      point: {
        radius: 4,
        hitRadius: 5,
        hoverRadius: 4,
        hoverBorderWidth: 1,
        borderWidth: 1,
      }
    }
  };

  //  BAR CHART
  // ==========================================================================================================
  public barChartOptions: ChartOptions = {
    responsive: true,
    
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{
     
    }], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = data.barChartData.labels;
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = data.barChartData.datasets ;
  public barChartColors: Color[]= data.barChartData.colors;


  //linechart 2
  public lineChartLegend2 = true;
  public lineChartType2 = 'line';
  public lineChartData2: ChartDataSets[] = data.lineChartData2.datasets;
  public lineChartLabels2: Label[] = data.lineChartData2.labels;
  public lineChartOptions2: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    elements: {
      point: {
        radius: 4,
        hitRadius: 5,
        hoverRadius: 4,
        hoverBorderWidth: 1,
        borderWidth: 1,
      },
    },
  };
  public lineChartColors2: Color[] = data.lineChartData2.colors;

// radar chart 
  public radarChartLabels: Label[] = data.radarChartData.labels;

  public radarChartData: ChartDataSets[] = data.radarChartData.datasets;
  public radarcolor: Color[] = data.radarChartData.colors;
  public radarChartOptions: RadialChartOptions = {
    responsive: true,
    elements: {
      point: {
        radius: 3,
        hoverRadius: 4,
        hoverBorderWidth: 1,
        borderWidth: 2,
      },
    },
  };
  public radarChartType: ChartType = 'radar';

  ngOnInit() {
    jQuery(document).ready( function () {
      // accessing easypiechart.min.js.
      jQuery('.project_post').easyPieChart({
        easing: 'easeOutBounce',
        barColor: '#54bc97',
        trackColor: '#f5f5f5',
        scaleColor: '#eaeaea',
        lineCap: 'square',
        lineWidth: 2,
        size: 30,
        animate: 1000,
        percent: 66
      });
    });
  }
  ngAfterViewInit() {
    jQuery('.sparkline').sparkline(
      [5, 10, 4, 4, 7, 5, 2, 8, 3, 4, 5], {
      type: 'line',
      width: '100%',
      height: '20',
      lineWidth: 1,
      lineColor: '#54bc97',
      fillColor: '#54bc97',
      highlightSpotColor: '#00797c',
      highlightLineColor: '#FF6E40',
      spotRadius: 1,
    });
    jQuery('.sparkbar').sparkline(
      [5, 10, 4, 4, 7, 5, 2, 8, 3, 4, 5], {
      type: 'bar',
      width: '100%',
      height: '20',
      lineWidth: 1,
      lineColor: '#54bc97',
      fillColor: '#54bc97',
      highlightSpotColor: '#00797c',
      highlightLineColor: '#FF6E40',
      spotRadius: 1,
    });
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {

  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(active);
  }



}
