import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UniversityRoutingModule } from './university-routing.module';
import { UniversityComponent } from './university.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [UniversityComponent],
  imports: [
    CommonModule,
    UniversityRoutingModule, SharedModule
  ]
})
export class UniversityModule { }
