import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, RadialChartOptions } from 'chart.js';
import { Label, Color, MultiDataSet } from 'ng2-charts';
@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.css']
})
export class UniversityComponent implements OnInit {
  customerGender = true;
  customerActivity = true;
  companyBudget = true;
  staffCommunication = true;
  customerQuality = true;
  showCustomerCost = true;
  // BAR CHART CONFIG
  // ===========================
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{ }], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  // ================================================================================================
  // LINE CHART CONFIG
  // ============================
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    }
  };
  public lineChartLegend = true;
  public lineChartType = 'line';
  // ==================================================================================================
  // RADAR CHART CONFIG
  // =========================
  public radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  public radarChartType: ChartType = 'radar';
  // ================================================================================================
  // DOUGHNUT CHART CONFIG
  // ===========================
  public doughnutChartType: ChartType = 'doughnut';
  // ================================================================================================
  // CUSTOMER COST PER MONTH PER YEAR
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013'];
  public barChartData: ChartDataSets[] = [
    { data: [10000, 20000, 50000, 15000, 60000, 30000, 5000, 20000], label: ' Cost Per Month (USD)',  },
    { data: [50560, 43070, 56789, 20000, 20056, 20300, 12000, 20000], label: 'Cost Per Year (USD)' }
  ];
  salesColor = [{ backgroundColor: '#33495d', barThickness: 60 }, { backgroundColor: '#eb5155', barThickness: 60 },
  ];
  // ==============================================================================================
  // CUSTOMER QUALITY OF SERVICE
  // ======================================
  public lineChartColors1: Color[] = [
    { // grey
      backgroundColor: '#32495cad',
      borderColor: '#32495c',
      pointBackgroundColor: '#32495c',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#32495c',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4
      
    },
    { // red
      backgroundColor: '#eb5155c9',
      borderColor: '#eb5155',
      pointBackgroundColor: '#eb5155',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#eb5155',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4

    },
    { // red
      backgroundColor: '#f5c13f75',
      borderColor: '#f7d438',
      pointBackgroundColor: '#f7d438',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f7d438',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4

    },


  ];
  public lineChartData1: ChartDataSets[] = [
    { data: [65, 59, 30, 41, 56, 55], label: 'Engineering Students' },
    { data: [28, 48, 40, 19, 86, 27], label: 'Commerce Students' },
    { data: [19, 86, 27, 65, 59, 30], label: 'Architecture Students' },
  ];
  public lineChartLabels1: Label[] = ['2010', '2011', '2012', '2013', '2014', '2015'];
  // ====================================================================================================
  // STAFF COMMUNICATION WITH CUSTOMERS
  // =============================================
  public barChartLabels1: Label[] = ['APR', 'MAY', 'JUN', 'JULY'];
  public barChartData1: ChartDataSets[] = [
    { data: [1000, 200, 500, 150], label: 'Professor Communication' },
    { data: [505, 430, 567, 200], label: ' Professor Listen Carefully' },
    { data: [600, 300, 500, 200], label: ' Professors Treated with Respect/Courtesy' },
    { data: [206, 200, 100, 200], label: 'Professors explained well to understand' }
  ];
  salesColor1 = [{ backgroundColor: '#fdb45d', barThickness: 60 }, { backgroundColor: '#4caf50', barThickness: 60 },
  { backgroundColor: '#33495d', barThickness: 60 }, { backgroundColor: '#eb5155', barThickness: 60 },
  ];
  // =====================================================================================================
  // COMPANY BUDGET
  // =======================
  public lineChartData: ChartDataSets[] = [
    { data: [6500, 5900, 8000, 4001, 5600, 5500, 4000], label: 'Student Health Budget' },
    { data: [2008, 4008, 4000, 1900, 8600, 2007, 9000], label: 'Staffing' },
    { data: [1800, 4800, 7700, 5000, 5000, 2700, 4000], label: 'Sports Equipments', },
    { data: [1400, 2200, 1200, 2700, 2340, 1700, 4120], label: 'Library', },
    { data: [1200, 1100, 1100, 1300, 5100, 5007, 4030], label: 'University Quaterly Events', }
  ];
  public lineChartLabels: Label[] = ['2010', '2011', '2012', '2013', '2014', '2015', '2016'];
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'transparent',
      borderColor: '#33495d',
      pointBackgroundColor: '#33495d',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#33495d',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4
    },
    { // dark grey
      backgroundColor: 'transparent',
      borderColor: '#f44336',
      pointBackgroundColor: '#f44336',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f44336',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: '#eb595d',
      pointBackgroundColor: '#eb595d',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#eb595d',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: '#4caf50',
      pointBackgroundColor: '#4caf50',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#4caf50',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: '#fdd835',
      pointBackgroundColor: '#fdd835',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fdd835',
      borderWidth: 2,
      pointBorderWidth: 1,
      pointRadius: 4
    },
  ];
  // ==============================================================================================================================
  // CUSTOMER DAILY ACTIVITY
  // ============================
  public radarChartLabels: Label[] = ['Campus  Counselling', 'Break Hours', 
  'Lectures', 'University Events', 'Sport Events', 'Practical Labs', 'Examinations'];

  public radarChartData: ChartDataSets[] = [
    { data: [65, 59, 90, 56, 35, 80, 40], label: 'Summer' },
    { data: [58, 48, 40, 69, 96, 57, 15], label: 'Winter' },
    { data: [38, 34, 60, 11, 56, 76, 23], label: 'Monsoon' }
  ];
  public radarcolor: Color[] = [
    { // red
      backgroundColor: '#32485cb8',
      borderColor: '#32485c',
      pointBackgroundColor: '#32485c',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#32485c',
      borderWidth: 2,
      pointRadius: 3,
      pointBorderWidth: 1
    },
    { // red
      backgroundColor: '#eb5054bf',
      borderColor: '#eb5054',
      pointBackgroundColor: '#eb5054',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#eb5054',
      borderWidth: 2,
      pointRadius: 3,
      pointBorderWidth: 1
    },

    { // grey
      backgroundColor: '#fdb45ceb',
      borderColor: '#fdb45c',
      pointBackgroundColor: '#fdb45c',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fdb45c',
      borderWidth: 2,
      pointRadius: 3,
      pointBorderWidth: 1
    },

  ];
  // ===============================================================================================================================
  // CUSTOMER GENDER COUNT
  // ===========================
  public doughnutChartLabels: Label[] = ['Female Staff Adults', 'Senior Staff Citizens', 'Male Staff Adults',];
  public doughnutChartData: MultiDataSet = [
    [350, 50, 400]
  ];
  doughnutColor = [{ backgroundColor: ['#33495d', '#eb5155', '#4caf50'] }];
  // ===============================================================================================================================
  chartClicked(event){

  }
chartHovered(event){

}
  constructor() { }

  ngOnInit() {
  }

}
