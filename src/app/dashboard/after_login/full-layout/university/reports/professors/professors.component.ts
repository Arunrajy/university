import { Component, OnInit,ViewChild } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets , Chart} from 'chart.js';

import { Label, Color, BaseChartDirective, MultiDataSet, } from 'ng2-charts';
declare var require: any;
const data: any = require('./professors-report.json');
@Component({
  selector: 'app-professors',
  templateUrl: './professors.component.html',
  styleUrls: ['./professors.component.css']
})
export class ProfessorsComponent implements OnInit {

  targetByYear = true;
  monthlyTarget = true;
  customerGender = true;
  // ==================================================================================
  // DOUGHNUT CHART CONFIG
  // ===========================
  public doughnutChartType: ChartType = 'doughnut';

  public doughnutChartLabels: Label[] = data.doughnutData.labels;
  public doughnutChartData: MultiDataSet = data.doughnutData.multiDatasets;
  public doughnutColor = data.doughnutData.colors;
  // ================================================================================
  // LINE CHART CONFIG
  // ============================
  public lineChartOptions1: ChartOptions = {
    responsive: true,
    elements: {
      line: {
        tension: 0.2,
        borderWidth: 10,
      },
      point:
      {
        radius: 11,
        hitRadius: 5,
        hoverRadius: 11,
        hoverBorderWidth: 2,
        borderWidth: 2,
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    }
  };

  public lineChartLegend1 = true;
  public lineChartType1 = 'line';

  // ===============================================================================================================================
  // MONTHLY TARGET INCOME
  // ==============================
  // COMPANY BUDGET
  // =======================
  public lineChartData1: ChartDataSets[] = data.lineChartData1.datasets;
  public lineChartLabels1: Label[] = data.lineChartData1.labels;
  public lineChartColors1: Color[] = data.lineChartData1.colors;
  public lineChartOptions2: ChartOptions = {
    responsive: true,

    elements: {
      line: {
        tension: 0,
      },
      point:
      {
        radius: 7,
        hitRadius: 7,
        hoverRadius: 7,
        hoverBorderWidth: 2,
        borderWidth: 2
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    hover: {
      mode: 'point',
      // onHover: (e, active) => {
      //   const point = this.myChart.getElementAtEvent(e);
      //   const x = e.target as HTMLInputElement;

      //   if (active.length > 0) {
      //     x.style.cursor = 'pointer';
      //   } else {
      //     x.style.cursor = 'default';
      //   }
      // }
    }
  };
  public lineChartLegend2 = true;
  public lineChartType2 = 'line';

  public lineChartData2: ChartDataSets[] = data.lineChartData2.datasets;
  public lineChartLabels2: Label[] = data.lineChartData2.labels;
  public lineChartColors2: Color[] = data.lineChartData2.colors;
  myChart: any;
  
  // @ViewChild('customChart') private chart1: BaseChartDirective
  // =====================================================================================================================================
  constructor() { }

  ngOnInit() {
    
  }
  chartHovered = (event) => {

  }
  chartClicked = (event) => {
  }
  ngAfterViewInit() {

    // let draw = Chart.controllers.line.prototype.draw;
    // Chart.controllers.line = Chart.controllers.line.extend({
    //   draw: function () {
    //     draw.apply(this, arguments);
    //     const ctx = this.chart.chart.ctx;
    //     const _stroke = ctx.stroke;
    //     ctx.stroke = function () {
    //       ctx.save();
    //       ctx.shadowColor = '#0000004d';
    //       ctx.shadowBlur = 4;
    //       ctx.shadowOffsetX = 0;
    //       ctx.shadowOffsetY = 6;
    //       _stroke.apply(this, arguments);
    //       ctx.restore();
    //     };
    //   },
    // });
    // const ctxus = document.getElementById('customer') as HTMLCanvasElement;
    // const y = ctxus.getContext('2d');
    // this.myChart = new Chart(y, {
    //   type: 'line',
    //   data: {
    //     labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
    //     datasets: [
    //       {
    //         data: [2, 5, 3, 3, 7, 4, 1, 6, 4, 5, 2, 4], label: 'Current Year',
    //         backgroundColor: '#33495d0f',
    //         borderColor: '#1F224B',
    //         pointBackgroundColor: '#fff ',
    //         pointBorderColor: '#1F224B',
    //         pointHoverBackgroundColor: '#fff',
    //         pointHoverBorderColor: '#1F224B', borderWidth: 2
    //       },
    //       {
    //         data: [1, 4, 3, 2, 5, 2, 1, 5, 4, 2, 2, 1], label: 'Last Year',
    //         backgroundColor: 'transparent',
    //         borderColor: 'transparent',
    //         pointBackgroundColor: '#ffff',
    //         pointBorderColor: '#eb5155',
    //         pointHoverBackgroundColor: '#fff',
    //         pointHoverBorderColor: '#ec8e72', borderWidth: 2
    //       }
    //     ]
    //   },
    //   options: this.lineChartOptions2
    // });
  }
}
