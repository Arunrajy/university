import { Component, OnInit } from '@angular/core';
// CHART IMPORTS
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color, BaseChartDirective, } from 'ng2-charts';
declare var require: any;
const data: any = require('./students-report.json');
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  customerSatisfaction = true;
  monthlySurvey = true;
  growthGraph = true;
  customerLead = true;
  //  BAR CHART 1
  // ==========================================================================================================
  public barChartOptions1: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{
    }], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels1: Label[] = data.barChartData1.labels ;
  public barChartType1: ChartType = 'bar';
  public barChartLegend1 = true;

  public barChartData1: ChartDataSets[] = data.barChartData1.datasets ;
  public barChartColor1 = data.barChartData1.colors;
  // line chart 1
  public lineChartLegend1 = true;
  public lineChartType1 = 'line';
  public lineChartOptions1: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    elements: {
      point: {
        radius: 4,
        hitRadius: 5,
        hoverRadius: 4,
        hoverBorderWidth: 1,
        borderWidth: 1,
      }
    },
  
  };
  public lineChartColors1: Color[] = data.lineChartData1.colors;
  public lineChartData1: ChartDataSets[] = data.lineChartData1.datasets;
  public lineChartLabels1: Label[] = data.lineChartData1.labels;

  // line chart 2
  // MONTHLY SURVEY
  public lineChartLegend2 = true;
  public lineChartType2 = 'line';
  public lineChartOptions2: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    elements: {
      point: {
        radius: 4,
        hitRadius: 5,
        hoverRadius: 4,
        hoverBorderWidth: 2,
        borderWidth: 1,
      }
    }
  };
  public lineChartData2: ChartDataSets[] = data.lineChartData2.datasets;
  public lineChartLabels2: Label[] = data.lineChartData2.labels;


  public lineChartColors2: Color[] = data.lineChartData2.color;

  // bar chart 2
  // CUSTOMER SATISFACTION LEVEL
  // ====================================
  public barChartType2: ChartType = 'bar';
  public barChartLegend2 = true;
  public barChartData2: ChartDataSets[] = data.barChartData2.datasets;
  public barChartColor2 = data.barChartData2.colors;
  public barChartLabels2: Label[] = data.barChartData2.labels;
  public barChartOptions2: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{
    }], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  constructor() { }

  ngOnInit() {
  }
  // =========================================================================================================
  // BAR CHART CONFIGS
  // ===========================
  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {

  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {

  }
  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    this.barChartData1[0].data = data;
  }
}
