import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './full-layout.component';


const routes: Routes = [
  { path: '', redirectTo: '/university/dashboard', pathMatch: 'full' },
  // =====================================================================================================================================
  // UNIVERSITY
  // ===============
  {
    path: 'university', component: FullLayoutComponent,
    children: [
    
      // DASHBOARD
      { path: 'dashboard', loadChildren: () => import('./university/dashboard/dashboard.module').then(m => m.DashboardModule) ,
       data: {
        title: 'Dashboard',
      }
      },
      // PROFESSORS
      {
        path: 'professors', loadChildren: () => import('./university/professor/professors/professors.module').
          then(m => m.ProfessorsModule),
          data: {
            title: 'Professors',
          }
      },
      {
        path: 'professor-add', loadChildren: () => import('./university/professor/professor-add/professor-add.module').
          then(m => m.ProfessorAddModule),
          data: {
            title: 'Add Professors',
          }
      },
      {
        path: 'professor-edit', loadChildren: () => import('./university/professor/professor-edit/professor-edit.module').
          then(m => m.ProfessorEditModule),
          data: {
            title: 'Edit Professors',
          }
      },
      {
        path: 'professor-profile', loadChildren: () => import('./university/professor/professor-profile/professor-profile.module').
          then(m => m.ProfessorProfileModule),
          data: {
            title: 'Professor Profile',
          }
      },
      // STUDENTS
      { path: 'students', loadChildren: () => import('./university/student/students/students.module').then(m => m.StudentsModule),
      data: {
        title: 'Students',
      }
      },
      {
        path: 'student-add', loadChildren: () => import('./university/student/student-add/student-add.module').
          then(m => m.StudentAddModule),
          data: {
            title: 'Add Student ',
          }
      },
      {
        path: 'student-edit', loadChildren: () => import('./university/student/student-edit/student-edit.module').
          then(m => m.StudentEditModule),
          data: {
            title: 'Edit Student ',
          }
      },
      {
        path: 'student-profile', loadChildren: () => import('./university/student/student-profile/student-profile.module').
          then(m => m.StudentProfileModule),
          data: {
            title: 'Student Profile',
          }
      },
      // REPORTS
      { path: 'students-report', loadChildren: () => import('./university/reports/students/students.module').then(m => m.StudentsModule),
      data: {
        title: 'Students Report',
      } },
      {
        path: 'professors-report', loadChildren: () => import('./university/reports/professors/professors.module').
          then(m => m.ProfessorsModule),
          data: {
            title: 'Professors Report',
          }
      },
      {
        path: 'university-report', loadChildren: () => import('./university/reports/university/university.module').
          then(m => m.UniversityModule),
          data: {
            title: 'University Report ',
          }
      },
      // MAIL
      { path: 'mail', loadChildren: () => import('./university/mail/mail.module').then(m => m.MailModule),
      data: {
        title: 'Mail ',
      }
      },
      // COURSES
      { path: 'courses', loadChildren: () => import('./university/courses/courses/courses.module').then(m => m.CoursesModule),
      data: {
        title: 'Courses ',
      }
      },
      { path: 'course-add', loadChildren: () => import('./university/courses/course-add/course-add.module').then(m => m.CourseAddModule),
      data: {
        title: 'Add Course ',
      }
      },
      {
        path: 'course-edit', loadChildren: () => import('./university/courses/course-edit/course-edit.module').
          then(m => m.CourseEditModule),
          data: {
            title: 'Edit Course ',
          }
      },
      {
        path: 'course-info', loadChildren: () => import('./university/courses/course-info/course-info.module').
          then(m => m.CourseInfoModule),
          data: {
            title: 'Course Info',
          }
      },
      // LIBRARY
      {
        path: 'library-assets', loadChildren: () => import('./university/library/library-assets/library-assets.module').
          then(m => m.LibraryAssetsModule),
          data: {
            title: 'Library Assets ',
          }
      },
      {
        path: 'library-asset-add', loadChildren: () => import('./university/library/library-assets-add/library-assets-add.module').
          then(m => m.LibraryAssetsAddModule),
          data: {
            title: 'Add Library Asset',
          }
      },
      {
        path: 'library-asset-edit', loadChildren: () => import('./university/library/library-assets-edit/library-assets-edit.module').
          then(m => m.LibraryAssetsEditModule),
          data: {
            title: 'Edit Library Asset ',
          }
      },
      // DEPARTMENT
      {
        path: 'departments', loadChildren: () => import('./university/departments/departments/departments.module').
          then(m => m.DepartmentsModule),
          data: {
            title: 'Departments ',
          }
      },
      {
        path: 'department-add', loadChildren: () => import('./university/departments/department-add/department-add.module').
          then(m => m.DepartmentAddModule),
          data: {
            title: 'Add Departments ',
          }
      },
      {
        path: 'department-edit', loadChildren: () => import('./university/departments/department-edit/department-edit.module').
          then(m => m.DepartmentEditModule),
          data: {
            title: 'Edit Departments ',
          }
      },
      // STAFF
      {
        path: 'staff-members', loadChildren: () => import('./university/staff/staff-members/staff-members.module').
          then(m => m.StaffMembersModule),
          data: {
            title: 'Staff Members ',
          }
      },
      {
        path: 'staff-member-add', loadChildren: () => import('./university/staff/staff-members-add/staff-members-add.module').
          then(m => m.StaffMembersAddModule),
          data: {
            title: 'Add Staff Member ',
          }
      },
      {
        path: 'staff-member-edit', loadChildren: () => import('./university/staff/staff-members-edit/staff-members-edit.module').
          then(m => m.StaffMembersEditModule),
          data: {
            title: 'Edit Staff Member ',
          }
      },
      {
        path: 'staff-member-profile', loadChildren: () => import('./university/staff/staff-members-profile/staff-members-profile.module').
          then(m => m.StaffMembersProfileModule),
          data: {
            title: 'Staff Member Profile ',
          }
      },
      // EVENT MANAGEMENT
      {
        path: 'event-management', loadChildren: () => import('./university/calendar-event/calendar-event.module').
          then(m => m.CalendarEventModule),
          data: {
            title: 'Event Management ',
          }
      },
      // UNIVERSITY CENTRES
      {
        path: 'university-centres', loadChildren: () => import('./university/university-centres/university-centres.module').
          then(m => m.UniversityCentresModule),
          data: {
            title: 'University Centers ',
          }
      },
      { path: '**', redirectTo: '/university/dashboard' },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FullLayoutRoutingModule { }
