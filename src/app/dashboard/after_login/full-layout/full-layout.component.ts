import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router,NavigationEnd} from '@angular/router';
import { map,filter,mergeMap } from 'rxjs/operators';
@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.css']
})
export class FullLayoutComponent implements OnInit {
  @ViewChild('myDiv') divView: ElementRef;
  pageTitle:any;
  constructor(public ef: ElementRef, public route: ActivatedRoute,    private router: Router,   ) {

    if (this.route.routeConfig.path === 'university') {
      this.sidebarValue = 'university';
    }
    
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    .pipe(map(() => this.route))
    .pipe(map(route => {
      while (route.firstChild) {
        route = route.firstChild;
      }
      return route;
    }))
    .pipe(filter(route => route.outlet === 'primary'))
    .pipe(mergeMap(route => route.data))
    .subscribe(event => {
      this.pageTitle = event.title;
    });
 
 
  }
  sidebarValue: any = 'crm';
  sidebar = true;
  show: any = [];
  log: boolean;
  email: boolean;
  notiShow = false;
  emailShow = false;
  chatShow = false;

  ngOnInit() {
  }
  sidebarClass = () => {
    if (this.sidebar == true) {
      return 'logo-bar';
    } else {
      return 'logo-bar-shift';
    }
  }
  chartHovered = () => {

  }
  // ==================================================================================================================================
  // SHOWN SIDE BAR
  // ======================
  sideNav = () => {
    if (this.sidebar == true) {
      this.sidebar = false;
      const x = document.getElementsByClassName('ps') as HTMLCollectionOf<HTMLElement>;
      x[(x.length - 1)].style.overflow = 'visible';
    } else {
      this.sidebar = true;
      this.chatShow = false;
      const x = document.getElementsByClassName('ps') as HTMLCollectionOf<HTMLElement>;
      x[(x.length - 1)].style.overflow = 'hidden';
    }

  }
  // ===================================================================================================================================
  // SHOWN ELEMENS In SIDE NAVIGATION
  // ==========================================
  Show = (event) => {
    if (this.show[event] == undefined) {
      this.show[event] = true;
      if (this.show.length != 0) {
        this.show.forEach((element, i) => {
          if (event != i) {
            this.show[i] = false;
          }
        });
      }
    } else if (this.show[event] == true && this.show.length != 0) {
      this.show.forEach((element, i) => {
        this.show[i] = false;
      });
    } else {
      if (this.show[event] == false && this.show.length != 0) {
        this.show.forEach((element, i) => {
          if (event == i) {
            this.show[i] = true;
          } else {
            this.show[i] = false;
          }
        });
      }
    }
  }
  // ==================================================================================================================================
  // NORIFICATION SHOW FUNCTION
  // ================================
  notification = () => {

    if (this.notiShow == true) {
      this.notiShow = false;
    } else {
      this.notiShow = true;
      this.emailShow = false;
    }
  }
  // EMAIL SHOW FUNCTION
  // ===========================
  emailShw = () => {
    if (this.emailShow == false) {
      this.emailShow = true;
      this.notiShow = false;
    } else {
      this.emailShow = false;
    }
  }
  // =========================================================================================================================================
  // SHOWEN CHAT
  // ==================
  showChat = () => {
    if (this.chatShow == false) {
      this.chatShow = true;
      this.sidebar = false;
    } else {
      this.chatShow = false;
    }
  }
  // ======================================================================================================================================
  display = ($event: any) => {
    this.chatShow = $event;
  }
  emailDropDownclose = () => {
    this.emailShow = false;
  }
  notificationDropdownclose() {
    this.notiShow = false;
  }
  value = () => {
    return {
      chatShow: this.chatShow,
      selected: this.sidebarValue
    };
  }
}
