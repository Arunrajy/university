import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: '', loadChildren: () => import ('./dashboard/before_login/blank/blank.module').then(m => m.BlankModule)},
  { path: '', loadChildren: () => import('./dashboard/after_login/full-layout/full-layout.module').then(m => m.FullLayoutModule), },
  // { path: '**', redirectTo: '/university/university/dashboard' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
